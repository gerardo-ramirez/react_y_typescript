const path = require("path");
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLwebpackplugin=  require('html-webpack-plugin');
module.exports = {
    entry: './src/index.tsx',
    output:{
        path: path.resolve(__dirname, 'build'),
        filename: 'bundel.js'
    },
    resolve:{
        extensions: ['.ts','.tsx','.js','.jsx','.json'],
    },
    module:{
        rules:[
            {
                test: /\.tsx?$/,
                loader:'awesome-typescript-loader',
            },
            {
                test: /\.scss?$/,
                use:[
                    miniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader',
            }
        ]
    },
    plugins : [
        new HTMLwebpackplugin({
            template:'src/index.html'
        }),
        new miniCssExtractPlugin('style.css')
    ],
    devtool: 'soure-map',

};
