import * as  React from 'react'; // importar todo desde react
import * as ReactDOM from 'react-dom'; //importar todo desde react-dom 
//importamos estilos:
import './styles/main.scss';

import { App } from './components/app';
const app = document.getElementById("app");
ReactDOM.render(<App title="Hola Gera"/>, app);
