import * as React from 'react';
import TaskForm from './TaskForm';
import TaskList from  './TaskList';
import {ITask} from './task';
//en el tsconfig le pedimos que los datos no sean implicitos .
//entonces le pasamos los datos que va a recibir el componente  :
//le pasamos como props una interface
//y tambien una interface de estado.
export class App extends React.Component<IProps, IState> {

    //creamos el constructor: porque al pasarle IState obligamos a que declare un estado task. 
   //nuevamente el tipo de props no puede ser implicito por eso :IProps.
    constructor(props :IProps){
        super(props);//hereda las propiedades de React. 
        this.state={
            tasks:[]
        }
    }
    AddNewTask(task:ITask){
        this.setState({
            tasks: [...this.state.tasks, task]
        },()=>console.log(this.state));
    };
    //creo un afuncion para eliminar tareas:
    deleteATask(id:number){
        //CREo una constante task
        const tasks : ITask[]= this.state.tasks.filter(
            //recorro el arreglo y guado las tareas de diferente id contra el id del parametro
            (task: ITask)=> task.id !==id
        )
        //vuelvo a setear el estado con el nuevo arreglo.
        this.setState({tasks});
    }
    render(){

        return(
            <div>
                <nav className="navbar navbar-light bg-light">
                    <a className="navbar-brand" href="/">
                        {this.props.title}
                    </a>
                </nav>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <TaskForm AddNewTask={this.AddNewTask.bind(this)}/>
                        </div>
                        <div className="col-md-8">
                            <TaskList tasks={this.state.tasks} deleteATask = {this.deleteATask.bind(this)}/>
                        </div>
                    </div>
                </div>
                

            </div>
        )
    }

    
}
//interface es la forma de pasarle propiedades, si tienen ? al final quiere decir que no son obligatoriar.
//define que propiedades va a recibir el componente:
interface IProps{
    title: string;
    subTitle?:string;

}
//podemos crear una interface para el estado:
interface IState {
    tasks: ITask[];
}