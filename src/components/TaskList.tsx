import React from 'react';
//importamos el tipo para la interfaz:
import {ITask} from './task';


class TaskList extends React.Component<ITaskListProp, any> {
    //ESPECIFICAMOS QUE TIPO DE ELEMENTO VOY A RENDERIZAR:
    render(): JSX.Element[]{
        return this.props.tasks.map((task: ITask, i: number)=>{
            return (
                <div className="col-md-4 mt-2" key={ task.id}>
                    <div className="card card-body">
                    <h3>{task.title}</h3>
                    <p>
                    {task.description}
                    </p>
                    <button className="btn btn-danger btn-block"
                    onClick={()=>this.props.deleteATask(task.id)}
                    >
                        Delete
                    </button>
                    </div>

                </div>
            )
        })

        

    }
}
interface ITaskListProp{
    tasks: ITask[];
    deleteATask : (id:number)=>void;
}
export default TaskList;