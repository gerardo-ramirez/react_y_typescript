import React from 'react';
import {ITask} from './task';


class TaskForm extends React.Component<ITaskFormProps,any>{
constructor(props: ITaskFormProps){
    super(props);
    this.state = {
        title:'',
        description:''
    };
}
//creamos una funcion para saber cuando se guarda la tarea:
    getCurrentTimesTamp(): number{
        const date = new Date();
        return date.getTime();
    }

    handleNewTask(e: React.FormEvent<HTMLFormElement>){
    e.preventDefault();
    //CREAMOS EL ARGUMENTO DE TIPO TAREA:
    const newTask : ITask ={
        id: this.getCurrentTimesTamp(),
        title:this.state.title,
        description: this.state.description,
        completed: false
    }
    //ESTA FUNCION ESPERA UN ARGUMENTO DE TIPO TAREA
    this.props.AddNewTask(newTask);
    //limpiar estado:
    this.setState({
        title:'',
        description:''
   
});

}
    handleInputChange(e: React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>){
        const {name,value} = e.target;
        
        this.setState({
            [name ]: value
        })
        
    }
render(){
    return(
        <div className="card card-body">
            <form onSubmit={e=> this.handleNewTask(e)} >
                <input onChange={e=>this.handleInputChange(e)}
                value={this.state.title}
                 name='title'  type="text" className="form-control" placeholder="task title"></input>
               
                    <div className="form-group">
                        <textarea 
                        name='description'
                        onChange={e=>this.handleInputChange(e)}
                        className="form-control"
                placeholder="Task Description"
                value={this.state.description}
                > </textarea>
                    </div>

                
                <button type="submit" className="btn btn-primary btn-block"> Save </button>
            </form>
        </div>
    )
}

}
//definimos las propiedades que puede recibir este componente.
//la creamos en el archivo task.ts y la importamos .
interface ITaskFormProps{
    AddNewTask: (Task : ITask)=> any;
}
//creamos una interface para el estado:
interface IStateTask {
    title: string;
    description: string;
}

export default TaskForm;
